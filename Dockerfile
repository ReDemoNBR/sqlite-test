FROM ubuntu:latest

RUN DEBIAN_FRONTEND=noninteractive && \
    apt update && \
    apt upgrade --yes --no-install-recommends && \
    apt install --yes --no-install-recommends sudo apt-utils curl wget tzdata gnupg-agent software-properties-common ca-certificates && \
    curl -sL https://deb.nodesource.com/setup_16.x | bash - && \
    apt update && \
    apt install --yes --no-install-recommends nodejs && \
    mkdir --parents /opt/sqlite-test

WORKDIR /opt/sqlite-test

COPY ./ ./

RUN npm install --production --no-fund --no-audit

EXPOSE 8080

CMD ["node", "server/index.js"]
