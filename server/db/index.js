import Sequelize from "sequelize";
import {SQLITE_DB_PATH, DB_NAME, DB_USER, DB_PASSWORD} from "../env.js";

export default new Sequelize({
    database: DB_NAME,
    username: DB_USER,
    password: DB_PASSWORD,
    dialect: "sqlite",
    storage: SQLITE_DB_PATH,

    define: {
        timestamps: true,
        createdAt: "created",
        updatedAt: "updated",
        deletedAt: "deleted",
        paranoid: false,
        underscored: true,
        freezeTableName: true
    }
});
