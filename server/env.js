import "dotenv/config";

const {env} = process;

export const SQLITE_DB_PATH = env.SQLITE_DB_PATH || "./db.sql";
export const DB_NAME = env.DB_NAME || "my-database";
export const DB_USER = env.DB_USER || "user";
export const DB_PASSWORD = env.DB_PASSWORD || "password";

export default {
    SQLITE_DB_PATH,
    DB_NAME,
    DB_USER,
    DB_PASSWORD
};
