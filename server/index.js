import Express from "express";
import db from "./db/init.js";
import routes from "./routes/index.js";

await db();

const app = Express();
app.use(routes);
const server = app.listen(8080);
server.setTimeout(60000);

server.once("listening", ()=>console.info("listening on port 8080"));

export default server;
