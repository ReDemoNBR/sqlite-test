import Item from "../db/item.js";

export default async(req, res, _next)=>{
    try {
        const item = await Item.create();
        return res.status(201).send(item);
    } catch(e) {
        return res.status(500).send(e);
    }
}
