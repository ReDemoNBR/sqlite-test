import {Router} from "express";
import read from "./read.js";
import list from "./list.js";
import generate from "./gen.js";

const router = Router();

router.get("/:id", read);
router.get("/", list);
router.post("/", generate);

export default router;
