import Item from "../db/item.js";

export default async(req, res, _next)=>{
    try {
        const items = await Item.findAll({
            order: [["id", "ASC"]]
        });
        if (!items?.length) return res.status(404).send({error: "not found"});
        return res.send(items);
    } catch(e) {
        return res.status(500).send(e);
    }
}
