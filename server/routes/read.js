import Item from "../db/item.js";

export default async(req, res, _next)=>{
    const {params: {id}} = req;
    try {
        const item = await Item.findOne({where: {id}});
        if (!item) return res.status(404).send({error: "not found"});
        return res.send(item);
    } catch(e) {
        return res.status(500).send(e);
    }
}
